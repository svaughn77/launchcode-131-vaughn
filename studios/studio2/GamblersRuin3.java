package studio2;

import cse131.ArgsProcessor;

public class GamblersRuin3 {

	public static void main(String[] args) {
		
		// TODO Auto-generated method stub
		ArgsProcessor ap = new ArgsProcessor(args);

		double startAmount = ap.nextDouble("What is the amount of money to start?");
		double winChance = ap.nextDouble("What is the probability that you win a gamble?");
		double winAmount = ap.nextDouble("How much money to determine a WIN? ");
		int totalPlays = ap.nextInt("What is the number of times you play");
		String winPlay = "";
		double ruin = 0.0;		//String losePlay = "Lose";
		int losecounter = 0;
		//int rounds = 0;  Does not work

		double lossChance = 1 - winChance;

		

		if (lossChance != winChance){
			//double Ruin = math.pow(lossChance/winChance,startAmount) - 
			ruin = (Math.pow((lossChance/winChance), startAmount)-
					Math.pow((lossChance/winChance),winAmount)) / (1 - Math.pow((lossChance/winChance),winAmount)); }
		else 
			if (lossChance == winChance){
				ruin = 1 - startAmount / winAmount;}
		
		
		
		for(int t =1; t<= totalPlays; t++){
			int rounds =0;
			
			double money = startAmount;
			while(money >0 && money < winAmount){
				rounds = rounds+1;
								
				if (Math.random() < winChance){  //Considered a WIN!
					money = money + 1;}  //adding a dollar for the win

				else {  //Considered a LOSE!
					money =  money - 1;  //subtracting a dollar for the lose

								}
				//System.out.println("Testing rounds counter:"+ rounds);  This counter printout works
				}
			if( money == 0 ){
				winPlay = "LOSE";
		
			losecounter = losecounter + 1;}
			else {
				winPlay= "WIN";					
			}
			
						
			System.out.println("Simulation " + t + ":  "  + rounds + " rounds    "  + winPlay);
			
		
	}
		System.out.println("Losses:   "+ losecounter + "     Simulations:     "+ totalPlays);
		System.out.println(ruin);
	
}

}


