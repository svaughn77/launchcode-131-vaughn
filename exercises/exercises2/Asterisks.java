package exercises2;

import cse131.ArgsProcessor;

public class Asterisks {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArgsProcessor ap = new ArgsProcessor(args);
		int x = ap.nextInt("Number of Rows?");
		int y = ap.nextInt("Number of Columns?");
		
		for(int i = 0; i<x; i++) {
			for (int j = 0; j< y; j++) {
				//System.out.println("*");
				System.out.print("*");
			}
			System.out.println();
		}

	}

}
