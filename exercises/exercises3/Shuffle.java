package exercises3;

public class Shuffle {

	public static void main(String[] args) {
		String[] original = { "A", "B", "C", "D",
				"E", "F", "G", "H"
		};

		// print out original array
		for (int i=0; i < original.length; ++i) {
			System.out.println("Original at " + i + " is " + original[i]);
		}

		//
		// Follow the instructions on the web page to make a copy of
		// the original array, named shuffled, but with its elements
		// permuted from the original array.  The result is that the
		// shuffled array contains the same strings, but in a randomized
		// order.
		//

		
		//declare shuffled array, same size as original array
		String [] shuffled = new String [original.length];
				for(int i = 0; i < original.length; i++) {
					//shuffled[i] = Math.random(original);
				
					//2 sysouts commented out
				//	System.out.println("Original array  " + original[i]);
				//	System.out.println("Shuffled array  " + shuffled[i]);
				}
				System.out.println();
		//Exercise:  Iterate backwards over the shuffled array to fill it in
				for (int i = original.length-1 ;  i >= 0; --i ){
					//c = integer from 0.....i
					int c = (int)(Math.random()* (i+1));
				//  opt 1: 
					shuffled[i] = original[c];
											
						
				//move all the cards up from c+1 to length-1
				//for(int j =c; j<shuffled.length-1; j++){
					//original[j] = original [j+1];
					
					
					//****opt 2;   SWAP
					String t = original[i];
					original[i] = original[c];
					original[c] =t;
					
				}
				//}
				for (int i = 0; i < original.length; i++) {				
				System.out.println("Shuffled array now at  " +  i + " is " + shuffled[i] + Math.random()); 
	}
				for(int i = 0; i< original.length; i++) {
					System.out.println("Now our original at   " + i + " is " + original[i] );
				}
	
}
}
