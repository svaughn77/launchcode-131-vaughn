package lab5;

public class Lab5Methods {

	public static int sumDownBy2 (int n){

		int sum = 0;
		int x = 2;

		if(n<=0||sum <0){
			return 0;
		}
		else if(n>0){
			sum = sum + n;
			while(sum>=0 && (n-x)> 0){
				//for (int sum >=0){
				// (i = 0;  )
				sum= sum + (n-x);
				x = x+2;

			}

		}
		return sum;
	}
	
	public static double harmonicSum (int n){
		double sum = 0.0;

		if(n<=0){
			return 0.0;
		}
		else if(n==1){
			sum = sum +1;}
		
		else if (n>1){
			sum = sum +1.0;

			for(int i = 1; i<n; i++){
				sum = sum + (double)1.0/(i+1);
				}
	}
		return sum;
		
	}
	
	
	public static double geometricSum (int k){
		double sum = 0.0;
		
		if(k <0){
			return 0;
		}
		for(int i = 0; i<=k; i++){
		sum = sum + (double)1.0/(Math.pow(2, i));}
		
		
		return sum;
		
	}
	
	
	public static int multPos (int j,int k){
		int sum =0;
		
		if(j<0 ||k < 0){
			return 0;
		}
		
		for (int i =1; i<=k; i++){
			sum = sum + j;
		}
		return sum;
	}
	
	
	public static int mult(int j, int k){
		
		int product = Lab5Methods.multPos(Math.abs(j), Math.abs(k));
		
		if(j< 0 && k<0){
			return product;
		}
		else if(j<0 || k<0 ){
			return (product * -1);
			
		}
		else {
			return product;
		}
	}
	
	public static int expt (int n, int k ){
		int prod = 1;
		if(k<0){
			return 0;
		}
		
		for(int i = 0; i<k; i++){
			prod = prod * n;
		}
		
		return prod;
	}
	
	
	
	
	
	
	
	
	
	
}
