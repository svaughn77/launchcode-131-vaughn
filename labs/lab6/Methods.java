package lab6;

public class Methods {
	
	//
	// In this class, implement the f and g functions described on the lab page
	//
	
	public static int f(int x){
		//base case
		//if(x<=0){
		//	return 0;
		
		if(x>100){
			return x-10;
		}
		else{
			return f(f(x+11));
			//int b = functionF(a);
			//return a;
		}
	}
	
	public static int g(int x, int y){
		int fin =0;
		//base case
		if(x==0){
			fin = y+1;
			
			
		}
		if(x>0 && y==0){
			fin = g(x-1,1);
		}
		if(x>0 && y>0){
			fin = g(x-1,g(x,y-1));
		}
		return fin;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		//
		// from here, call f or g with the appropriate parameters
		//
		int ans = f(87);
		int ans2 = g(1,0);
		System.out.println(ans);
		System.out.println(ans2);
	}

}
