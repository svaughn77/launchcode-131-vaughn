package lab1;
import cse131.ArgsProcessor;

public class Nutrition {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArgsProcessor ap = new ArgsProcessor(args);
		
		String name = ap.nextString("Please provide the name of the food:");
		double carbs = ap.nextDouble("Please provide the number of carbs in the food:");
		double fat = ap.nextDouble("Please provide the number of grams of fat in the food:");
		double protein = ap.nextDouble("Please provide the number of grams of protein in the food");
		int statedCals = ap.nextInt("Please provide the number of calories stated on this food's label");

		
		
		
		double carbsCal = carbs * 4;
		//double fatCal = Math.round(fat * 9);
		double fatCal = fat * 9;
		double proteinCal = protein * 4;
		
		double totCal = carbsCal + fatCal + proteinCal;
		//double unavailableCal = totCal - statedCals;
		//double fiber = unavailableCal/4;
		double unavailableCal = Math.round((totCal - statedCals)*1000.0) /10.0;
		double fiber = unavailableCal/4;
		
		double carbsCalrounded = Math.round(carbsCal *100.0)/100.0;
		double fatCalrounded = Math.round(fatCal *100.0)/100.0;
		double proteinCalrounded = Math.round(proteinCal * 100.0)/100.0;
		double fiberrounded = Math.round(fiber *100.0)/100.0;
		
		double percentCalCarb = Math.round((carbsCal/totCal)*1000.0) /10.0;
		//double percentCalFat =( fatCal/totCal) *100;
		//double percentCalProtein = (proteinCal/totCal) * 100;
		double percentCalFat = Math.round((fatCal/totCal)*1000.0) /10.0;
		double percentCalProtein = Math.round((proteinCal/totCal)*1000.0) /10.0;
		
		
		
			
		double lowCarbCriteria = .25 ;
		double lowFatCriteria = .15 ;
		boolean lowCarb = carbsCal/totCal <= lowCarbCriteria ;
		boolean lowFat = fatCal/totCal <= lowFatCriteria;
		
		//public class Randomness {
		//	public static void main(String[] args) {
				double t = Math.random ();
				boolean heads = t< .5;
			//}
		//}
		
		
		
		System.out.println(name + " has " + carbs + " grams of carbohydrates = " + carbsCalrounded + " Calories " + fat + " grams of fat = " + fatCalrounded + "Calories "+ protein + " grams of protein = " + proteinCalrounded + " Calories");
		
		System.out.println("This food is said to have " + statedCals + " (available) Calories. With " + unavailableCal + " unavailable Calories, this food has " + fiberrounded +" grams of fiber.");
		
		System.out.println("Approximately " + percentCalCarb + "%  of your food is from carbohydrates" + percentCalFat + "%  of your food is from fat" + + percentCalProtein + "%  of your food is from protein.");
		
		System.out.println("This food is acceptable for a low-carb diet? " + lowCarb);
		System.out.println("This food is acceptable for a low-fat diet? " + lowFat);
		System.out.println("By coin flip, you should eat this food? " + heads);
		
		
	}

}
