package lab4;

import cse131.ArgsProcessor;
import sedgewick.StdDraw;

public class BumpingBalls {

	public static void main(String[] args) {
		ArgsProcessor ap = new ArgsProcessor(args);

		int ballNum = ap.nextInt("How many balls will be used?");
		int iterateNum = ap.nextInt("How many iterations?");
		int N = ap.nextInt("How much history?");
		double dist = 0.0;
		double distSqrt = 0.0;

		// set the scale of the coordinate system
		StdDraw.setXscale(0, 1.0);
		StdDraw.setYscale(0, 1.0);

		// initial values  code from Bouncing Ball
		// rx = 0.480, ry = 0.860;     // position
		//double vx = 0.015, vy = 0.023;     // velocity
		double radius = .09; 
		// radius

		double rx = 0.480;   
		double ry = 0.860;   
		//double vx = 0.015;   
		//double vy = 0.023;  
		double vx = .195;
		double vy = .190;

		//System.out.println("RandomRX =" + rx );
		//System.out.println("RandomRY =" + ry);

		//Keeping a history of the ball's positions and velocities
		double[] histRX = new double[N];
		double[] histRY = new double[N];
		double[] histVX = new double[N];
		double[] histVY = new double[N];

		double[] balls = new double[ballNum];

		int cur = 0;



		// main animation loop
		while (true)  { 
			//for(int i = 0; i<iterateNum; i++){
			for(int m =0; m< ballNum; m++){

			System.out.println("Ball num = " + m);
			// bounce off wall according to law of elastic collision
			if (Math.abs(rx + vx) > 1.0 - radius) vx = -vx;
			if (Math.abs(ry + vy) > 1.0 - radius) vy = -vy;


			// update position
			//rx = (rx + vx); 
			//ry = (ry + vy); 
			rx = Math.random();
			ry = Math.random();
			//vx = Math.random();   //random velocity x
			//vy = Math.random();   //random velocity y


			histRX [m]= rx;
			histRY [m]= ry;
			histVX [m]= vx;
			histVY [m]=vy;
			
			
			
			dist = (Math.pow((histRX[m+1])-(histRX[m]), 2.0))+ (Math.pow((histRY[m+1])-(histRY[m]), 2.0));
			distSqrt = Math.sqrt(dist);
			
			
			//bounce off corner/wall when balls collide
			if (distSqrt <= (2*radius)) vx = -vx;{
				for(int i= 0; i<ballNum; i++){
					
					System.out.println("COLLISION!!!!!!!!!!!!!!!!!!!!!!!");
					System.out.println("distance = " + distSqrt);

					//histRX[m+1]=0;
					//histRX[m]=1;
					//histRY[m+1]=0;
					//histRX[m]=1;
					rx = 1;
					ry = 0;
					
					System.out.println("collide rx = " + rx);
					System.out.println("collide ry = " + ry);
				}
				//rx = 0;
				//ry = .95;
				//for( int m =0; m<N; m++){
				//for(int m =0; m<ballNum; m++){
				//vx = -vx;
				// = -vy;

				//rx = -rx;
				//ry = -ry;
			}//end of if
			if  (distSqrt >ballNum *radius){
				
				//rx = (rx + vx); 
				//ry = (ry + vy);
				rx = Math.random();
				ry = Math.random();
				
				System.out.println("No collision");
				System.out.println("NC rx = " + rx);
				System.out.println("NC ry = " + ry);

			}	

			}//end of for

			for( int m =0; m<N; m++){

				//histRX[m] =rx;
				//histRY[m] = ry;
				System.out.println("histRX =" + histRX[m] );
				System.out.println("histRY =" + histRY[m]);
				System.out.println("histVX =" + histVX[m]);
				System.out.println("histVY =" + histVY[m]);

				//for (int j = 0; j < ballNum; j++) { 
				//
			}


			//(int k = 0; k<=ballNum; k++){

			//while(true){

			// clear the background

			StdDraw.clear();

			//StdDraw.setPenColor(StdDraw.GRAY);  FROM BB
			//StdDraw.filledSquare(0, 0, 1.0);   FROM BB


			//draw ball on the screen

			//histRX [N]= rx;
			//histRY [N]= ry;
			//histVX [N]= vx;
			//histVY [N] = vy;
			//cur = cur +1;

			for(int t = 0; t<ballNum; t ++){
				StdDraw.setPenColor(java.awt.Color.ORANGE);
				StdDraw.filledCircle(Math.random(), Math.random(), .09);
				//StdDraw.filledCircle(rx, ry, radius); 
				//StdDraw.show(500);

			} //end of for loop

			// display and pause for 20 ms
			StdDraw.show(200);
			
			
			//rx = Math.random();
			//ry = Math.random();
			//vx = Math.random();   //random velocity x
			//vy = Math.random();   //random velocity y



		} //end of while loop
	}//end of static void



}//end of class







